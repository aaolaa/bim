DROP TABLE objectplacement CASCADE;
DROP TABLE localplacement CASCADE;
DROP TABLE point CASCADE;
DROP TABLE cartesianpoint CASCADE;
DROP TABLE curve CASCADE;
DROP TABLE boundedcurve CASCADE;
DROP TABLE polyline CASCADE;
DROP TABLE profiledef CASCADE;
DROP TABLE arbitraryclosedprofiledef CASCADE;
DROP TABLE representationitem CASCADE;
DROP TABLE geometricrepresentationitem CASCADE;
DROP TABLE solidmodel CASCADE;
DROP TABLE sweptareasolid CASCADE;
DROP TABLE extrudedareasolid CASCADE;
DROP TABLE representation CASCADE;
DROP TABLE shapemodel CASCADE;
DROP TABLE shaperepresentation CASCADE;
DROP TABLE productrepresentation CASCADE;
DROP TABLE productdefinitionshape CASCADE;
DROP TABLE propertysinglevalue CASCADE;
DROP TABLE simpleproperty CASCADE;
DROP TABLE axis2placement3d CASCADE;
DROP TABLE placement CASCADE;
DROP TABLE space CASCADE;
DROP TABLE buildingstorey CASCADE;
DROP TABLE direction CASCADE;
DROP TABLE geometricrepresentationitem CASCADE;
DROP TABLE complexproperty CASCADE;
DROP TABLE property CASCADE;
DROP TABLE propertyabstraction CASCADE;
DROP TABLE propertyset CASCADE;
DROP TABLE propertysetdefinition CASCADE;
DROP TABLE propertydefinitiono CASCADE;
DROP TABLE reldefinesbyproperties CASCADE;
DROP TABLE reldefines CASCADE;
DROP TABLE relaggregates CASCADE;
DROP TABLE reldecomposes CASCADE;
DROP TABLE relationship CASCADE;
DROP TABLE building CASCADE;
DROP TABLE site CASCADE;
DROP TABLE spatialstructureelement CASCADE;
DROP TABLE spatialelement CASCADE;
DROP TABLE product CASCADE;
DROP TABLE object CASCADE;
DROP TABLE project CASCADE;
DROP TABLE context CASCADE;
DROP TABLE objectdefinition CASCADE;
DROP TABLE root CASCADE;


CREATE TABLE root (
	id bigint NOT NULL,
	globalid character varying,
	ownerhistory bigint,
	name character varying,
	description character varying,
	CONSTRAINT root_pk PRIMARY KEY (id)		
);


CREATE TABLE objectdefinition (
	CONSTRAINT objectdefinition_pk PRIMARY KEY (id)	
)
	INHERITS (root);


CREATE TABLE context (
	objecttype character varying,
	longname character varying,
	phase character varying,
	representationcontexts bigint[],
	unitsincontext bigint,
	CONSTRAINT context_pk PRIMARY KEY (id)
)
	INHERITS (objectdefinition);


CREATE TABLE project (
	CONSTRAINT project_pk PRIMARY KEY (id)	
)
	INHERITS (context);


CREATE TABLE object (
	objecttype character varying,
	CONSTRAINT object_pk PRIMARY KEY (id)
)
	INHERITS (objectdefinition);


CREATE TABLE product (
	objectplacement bigint,
	representation bigint,
	CONSTRAINT product_pk PRIMARY KEY (id)
)
	INHERITS (object);


CREATE TABLE spatialelement (
	longname character varying,
	CONSTRAINT spatialelement_pk PRIMARY KEY (id)
)
	INHERITS (product);


CREATE TABLE spatialstructureelement (
	compositiontype character varying, 
	CONSTRAINT spatialstructureelement_pk PRIMARY KEY (id)
)
	INHERITS (spatialelement);


CREATE TABLE site (
	reflatitude integer[],
	reflongitude integer[],
	refelevation real,
	landtitlenumber character varying,
	siteaddress bigint,
	CONSTRAINT sit_pk PRIMARY KEY (id)
)
	INHERITS (spatialstructureelement);


CREATE TABLE building (
	elevationofrefheight real,
	elevationofterrain real,
	buildingaddress character varying,
	CONSTRAINT building_pk PRIMARY KEY (id)
)
	INHERITS (spatialstructureelement);


CREATE TABLE relationship (
	CONSTRAINT relationship_pk PRIMARY KEY (id)		
)
	INHERITS (root);


CREATE TABLE reldecomposes (
	CONSTRAINT reldecomposes_pk PRIMARY KEY (id)		
)
	INHERITS (relationship);


CREATE TABLE relaggregates (
	relatingobject bigint NOT NULL,  -- typ ENTITY IfcObjectDefinition
    relatedobjects bigint[] NOT NULL, -- typ ENTITY IfcObjectDefinition
	CONSTRAINT relaggregates_pk PRIMARY KEY (id)		
)
	INHERITS (reldecomposes);


CREATE TABLE reldefines (
	CONSTRAINT reldefines_pk PRIMARY KEY (id)		
)
	INHERITS (relationship);


CREATE TABLE reldefinesbyproperties (
    relatedobjects bigint[] NOT NULL, -- typ ENTITY IfcObjectDefinition
    relatingpropertydefinition character varying, -- typ SELECT IfcPropertySetDefinitionSelect 
	CONSTRAINT reldefinesbyproperties_pk PRIMARY KEY (id)		
)
	INHERITS (reldefines);


CREATE TABLE propertydefinition (
	CONSTRAINT propertydefinition_pk PRIMARY KEY (id)		
)
	INHERITS (root);


CREATE TABLE propertysetdefinition (
	CONSTRAINT propertysetdefinition_pk PRIMARY KEY (id)		
)
	INHERITS (propertydefinition);


CREATE TABLE propertyset (
	hasproperties bigint[] NOT NULL, -- typ ENTITY IfcProperty
	CONSTRAINT propertyset_pk PRIMARY KEY (id)		
)
	INHERITS (propertysetdefinition);


CREATE TABLE propertyabstraction (
	id bigint NOT NULL,
	CONSTRAINT propertyabstraction_pk PRIMARY KEY (id)		
);


CREATE TABLE property (
	name character varying NOT NULL,
	description character varying,
	CONSTRAINT property_pk PRIMARY KEY (id)		
)
	INHERITS (propertyabstraction);


CREATE TABLE complexproperty (
	usagename character varying NOT NULL,
	hasproperties bigint[] NOT NULL, -- typ ENTITY IfcProperty
	CONSTRAINT complexproperty_pk PRIMARY KEY (id)		
)
	INHERITS (property);


CREATE TABLE representationitem (
	id bigint NOT NULL,
	CONSTRAINT representationitem_pk PRIMARY KEY (id)		
);


CREATE TABLE geometricrepresentationitem (
	CONSTRAINT geometricrepresentationitem_pk PRIMARY KEY (id)		
)
	INHERITS (representationitem);


CREATE TABLE direction (
	directionratios double precision[] NOT NULL,
	CONSTRAINT direction_pk PRIMARY KEY (id)		
)
	INHERITS (geometricrepresentationitem);


CREATE TABLE buildingstorey (
	elevation double precision,
	CONSTRAINT buildingstorey_pk PRIMARY KEY (id)
)
	INHERITS (spatialstructureelement);
    
    
CREATE TABLE space (
	predefinedtype character varying,
	elevationwithflooring double precision,
	CONSTRAINT space_pk PRIMARY KEY (id)
)
	INHERITS (spatialstructureelement);
    
    
CREATE TABLE placement (
	location bigint,
	CONSTRAINT placement_pk PRIMARY KEY (id)
)
	INHERITS (geometricrepresentationitem);

    
CREATE TABLE axis2placement3d (
	axis bigint,
    refdirection bigint,
	CONSTRAINT axis2placement3d_pk PRIMARY KEY (id)
)
	INHERITS (placement);

CREATE TABLE simpleproperty (
	CONSTRAINT simpleproperty_pk PRIMARY KEY (id)
)
	INHERITS (property);


CREATE TABLE propertysinglevalue (  
   nominalvalue character varying,    --ifcvalue - typ select jak go zapisac jako string ALE nie działa z ifcvalue
   unit character varying, --ifcunit bylo
   CONSTRAINT propertysinglevalue_pk PRIMARY KEY (id) 
) 
   INHERITS (simpleproperty);  

   
CREATE TABLE productrepresentation (  
   id bigint NOT NULL,
   name character varying,
   description character varying,
   representations bigint[],
   CONSTRAINT productrepresentation_pk PRIMARY KEY (id) 
);
   
   
CREATE TABLE productdefinitionshape (  
   CONSTRAINT productdefinitionshape_pk PRIMARY KEY (id) 
) 
   INHERITS (productrepresentation); 


CREATE TABLE representation (
   id bigint NOT NULL,  
   contextofitems bigint,
   representationidentifier character varying,
   representationtype character varying,
   items bigint[],
   CONSTRAINT representation_pk PRIMARY KEY (id) 
);
   
 
CREATE TABLE shapemodel (  
   CONSTRAINT shapemodel_pk PRIMARY KEY (id) 
) 
   INHERITS (representation);


CREATE TABLE shaperepresentation (  
   CONSTRAINT shaperepresentation_pk PRIMARY KEY (id) 
) 
   INHERITS (shapemodel);


CREATE TABLE solidmodel (  
   CONSTRAINT solidmodel_pk PRIMARY KEY (id) 
) 
   INHERITS (geometricrepresentationitem);  

  
CREATE TABLE sweptareasolid (  
   sweptarea bigint,
   position bigint,
   CONSTRAINT sweptareasolid_pk PRIMARY KEY (id) 
) 
   INHERITS (solidmodel);  


CREATE TABLE extrudedareasolid (  
   extrudeddirection bigint,
   depth double precision,
   CONSTRAINT extrudedareasolid_pk PRIMARY KEY (id) 
) 
   INHERITS (sweptareasolid); 
 

CREATE TABLE profiledef (
   id bigint NOT NULL,
   profiletype character varying,
   profilename character varying,
   CONSTRAINT profiledef_pk PRIMARY KEY (id) 
);


CREATE TABLE arbitraryclosedprofiledef (  
   outercurve bigint,
   CONSTRAINT arbitraryclosedprofiledef_pk PRIMARY KEY (id) 
) 
   INHERITS (profiledef); 
  

CREATE TABLE curve (  
   CONSTRAINT curve_pk PRIMARY KEY (id) 
) 
   INHERITS (geometricrepresentationitem);  


CREATE TABLE boundedcurve (  
   CONSTRAINT boundedcurve_pk PRIMARY KEY (id) 
) 
   INHERITS (curve);  

 
CREATE TABLE polyline (  
   points bigint[],
   CONSTRAINT polyline_pk PRIMARY KEY (id) 
) 
   INHERITS (boundedcurve); 
 

CREATE TABLE point (  
   CONSTRAINT point_pk PRIMARY KEY (id) 
) 
   INHERITS (geometricrepresentationitem);

   
CREATE TABLE cartesianpoint (  
   coordinates double precision[],
   CONSTRAINT cartesianpoint_pk PRIMARY KEY (id) 
) 
   INHERITS (point);

  
CREATE TABLE objectplacement (
   id bigint NOT NULL,  
   CONSTRAINT objectplacement_pk PRIMARY KEY (id) 
);
   
   
CREATE TABLE localplacement (  
   placementrelto bigint,
   relativeplacement character varying, --SELECT ifcAxis2Placement, -- typ select --postgresql mówi ze błąd składni
   CONSTRAINT localplacement_pk PRIMARY KEY (id) 
) 
   INHERITS (objectplacement); 
 

