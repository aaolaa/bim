import psycopg2

# sprawdzenie połączenie i połączenie z bazą
try:
    connection = psycopg2.connect("host='localhost' port='5432' dbname='ifc4' user='postgres' password='ola'")
    print('Connected')
except psycopg2.OperationalError:
    print('Connection failed')


cursor = connection.cursor()
# wyciągnięcie wszystkich tabel z bazy
cursor.execute("SELECT tables.table_name FROM information_schema.tables WHERE table_schema='public'")
rows = cursor.fetchall()

database = {}
all_tables = set()
# wyciągnięcie kolumn dla każdej tabeli
for row in rows:
	cursor.execute("SELECT column_name FROM information_schema.columns WHERE table_schema='public' AND table_name=%s", (row[0],))
	columns_names = cursor.fetchall()
	all_columns = [column_name[0] for column_name in columns_names if column_name[0] != 'id']
	table_name_ifc = 'ifc' + row[0]
	all_tables.add(table_name_ifc)
	# zapisanie kolumn do słownika, gdzie kluczem jest nazwa tabeli
	database[table_name_ifc] = all_columns

def get_next_attribute(string):
	'''
	otrzymuje string i zwraca kolejny atrubut i ogon tekstu
	'''
	if string[0] != '(' and string[:3] != 'IFC':
		poz = string.find(',')
		if poz != -1:
			return string[:poz], string[poz+1:]
		else:
			return string, ''
	elif string[:3] != 'IFC':
		poz = string.find(')')
		return '{'+string[1:poz]+'}', string[poz+2:]
	else:
		poz = string.find(')')
		return string[:poz], string[poz+2:]
		

def get_attributes(string):
	'''
	generuje wszystkie atrybuty, które zapisane są w podanym stringu
	'''
	string = string.replace('#', '')
	while len(string) > 0:
		next_attr, string = get_next_attribute(string)
		yield None if next_attr == '$' else next_attr

# wczytanie danych z pliku IFC
with open('HITOS_Architectural_2006-10-25.ifc') as file:
	for row in file:
		if row[0] == '#':
			number_and_others= row.split('=', 1)
			id_number = number_and_others[0][1:]
			class_name_and_others = number_and_others[1].split('(', 1)
			class_name = class_name_and_others[0].lower()[1:]
			columns_value = {}
			if class_name in all_tables:
				columns_db = database[class_name]
				start = class_name_and_others[1][:-3]
				list_value = list(get_attributes(start))
				procent_s = ''
				for i in range(len(columns_db)):
					if i != len(columns_db) - 1:
						procent_s += '%s,'
					else:
						procent_s += '%s'
				# zapisanie danych z IFC do bazy
				sql_insert = "INSERT INTO {} (id, {}) VALUES ({}, {})".format(class_name[3:], ','.join(columns_db), id_number, procent_s)
				print(sql_insert)

				print(list_value)
				cursor.execute(sql_insert, (*list_value,))

cursor.close()
